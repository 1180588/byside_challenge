import { Ficheiro } from "./Ficheiro";
import { Permissoes } from "./Permissoes";


export class Pasta{

    ficheiros: Ficheiro[]
    pastas: Pasta[]
    localizacao: string
    dataCriacao: Date
    tamanho: number
    permissoes: Permissoes

    public constructor(ficheiros: Ficheiro[], pastas: Pasta[], localizacao: string, dataCriacao: Date,
        tamanho: number, permissoes: Permissoes){
        this.ficheiros=ficheiros
        this.pastas=pastas
        this.localizacao=localizacao
        this.dataCriacao=dataCriacao
        this.tamanho=tamanho
        this.permissoes=permissoes
    }

setLocalizacao(localizacao: string){
    this.localizacao=localizacao
}

}