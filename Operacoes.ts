import { Ficheiro } from "./Ficheiro";
import { Pasta } from "./Pasta";
import { Permissoes } from "./Permissoes";

export class Operacoes{

public criarFicheiro(localizacao: string, tamanho: number, permissoesLeitura: boolean, permissoesEscrita: boolean,
    permissoesExecucao: boolean){
        return new Ficheiro(localizacao,new Date(),new Date(),new Date(),tamanho,new Permissoes(permissoesLeitura,
            permissoesEscrita,permissoesExecucao))
    }

public criarPasta(ficheiros: Ficheiro[], pastas: Pasta[], localizacao: string, tamanho: number, permissoesLeitura: boolean, permissoesEscrita: boolean,
    permissoesExecucao: boolean){
        return new Pasta(ficheiros, pastas, localizacao,new Date(),tamanho,new Permissoes(permissoesLeitura,
            permissoesEscrita,permissoesExecucao))
    }

public moverPasta(pasta: Pasta, localizacao: string){
    pasta.setLocalizacao(localizacao)
}

public moverFicheiro(ficheiro: Ficheiro, localizacao: string){
    ficheiro.setLocalizacao(localizacao)
}

public copiarFicheiro(ficheiro: Ficheiro){
    return new Ficheiro(ficheiro.localizacao,new Date(), new Date(), new Date(), ficheiro.tamanho,
    ficheiro.permissoes)
}

public copiarPasta(pasta: Pasta){
    return new Pasta(pasta.ficheiros, pasta.pastas, pasta.localizacao, new Date(), pasta.tamanho,
    pasta.permissoes)
}

}