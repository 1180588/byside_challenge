

export class Permissoes{

    permissoesLeitura: boolean
    permissoesEscrita: boolean
    permissoesExecucao: boolean

    public constructor(permissoesLeitura: boolean, permissoesEscrita: boolean,
        permissoesExecucao: boolean){
            this.permissoesLeitura = permissoesLeitura
            this.permissoesEscrita = permissoesEscrita
            this.permissoesExecucao = permissoesExecucao
        }


}