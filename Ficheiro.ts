import { Permissoes } from "./Permissoes"


export class Ficheiro{
    localizacao: string
    dataCriacao: Date
    dataUltimoAcesso: Date
    dataUltimaModificacao: Date
    tamanho: number
    permissoes: Permissoes

public constructor(localizacao: string, dataCriacao: Date, dataUltimoAcesso: Date,
    dataUltimaModificacao: Date, tamanho: number, permissoes: Permissoes){
        this.localizacao=localizacao
        this.dataCriacao=dataCriacao
        this.dataUltimoAcesso=dataUltimoAcesso
        this.dataUltimaModificacao=dataUltimaModificacao
        this.tamanho=tamanho
        this.permissoes=permissoes
    }

setLocalizacao(localizacao: string){
    this.localizacao=localizacao
}

}